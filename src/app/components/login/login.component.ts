import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AdminServiceService } from '../../service/admin-service.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  login: FormGroup;
  register: FormGroup;
  constructor(private adminService: AdminServiceService,
    private router: Router, private msg: ToastrService
    ) { }

  ngOnInit() {
    this.login = new FormGroup({
      email: new FormControl(null),
      password: new FormControl(null)
    });

    this.register = new FormGroup({
      email: new FormControl(null, {
        validators: [Validators.required, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]
      }),
      name: new FormControl(null, {
        validators: [Validators.required]
      }),
      address: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(10)]
      }),
      phone: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(10)]
      }),
      password: new FormControl(null, {
        validators: [Validators.required, Validators.pattern(/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/), Validators.minLength(8)]
      }),
      cpassword: new FormControl(null)
    })
  }

  showError(msg) {
    this.msg.error(msg, 'Error', {
      timeOut: 5000, progressBar: true
    })
  }
  showSuccess(msg) {
    this.msg.success(msg, 'Success', {
      timeOut: 5000, progressBar: true
    })
  }

  get name() {
    return this.register.get('name')
  }
  get email() {
    return this.register.get('email')
  }
  get phone() {
    return this.register.get('phone')
  }
  get password() {
  return this.register.get('password')
  }
  get address() {
    return this.register.get('address')
  }

  loginData() {
    const passRegex = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!passRegex.test(this.login.value.password) || !emailRegex.test(this.login.value.email)) {
      this.showError('Format of Credentials not valid !');
      return;
    }
    this.adminService.login(this.login.value)
    .subscribe(data => {
      if(data.success === 1) {
        localStorage.setItem('token', data.data.token);
        this.showSuccess('Successfully Logged in !')
        this.router.navigate(['/home/dashboard']);
      }
      else {
        this.showError(data.data)
      }
    })

  }

registerUser() {
  if(this.register.value.password !== this.register.value.cpassword) {
    this.showError('Passwords do not match !');
    return;
  }
if(!this.register.valid) {
this.showError('Please Fill the form Properly');
return;
}
this.adminService.registerUser(this.register.value)
.subscribe(data => {
  if(data.success === 1) {
    this.showSuccess(data.data)
  }
  else {
    this.showError(data.data)
  }
  document.getElementById('cancelModal').click();
  this.register.reset();
})
}


}
