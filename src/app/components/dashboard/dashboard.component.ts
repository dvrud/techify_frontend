import { Component, OnInit } from '@angular/core';
import { AdminServiceService } from '../../service/admin-service.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
user: any = {
name: '',
address: '',
admin: false,
email: '',
phone: ''
};
loading:boolean = true;
users: Array<any> = [];
selectedUser: any = {
  name: '',
address: '',
email: '',
phone: ''
}
  constructor(private admin: AdminServiceService, private msg: ToastrService) { }

  ngOnInit() {
    this.admin.getUser()
    .subscribe(data => {
      this.user = data.data;
      if(data.data.admin) {
        this.admin.getUsers()
        .subscribe(data => {
          if(data.success === 1) {
            this.users = data.data
          }
          else {
            this.showError('Error in fetching users');
          }
         this.loading = false;
        })
      }
    })
  }

   showError(msg) {
    this.msg.error(msg, 'Error', {
      timeOut: 5000, progressBar: true
    })
  }
  showSuccess(msg) {
    this.msg.success(msg, 'Success', {
      timeOut: 5000, progressBar: true
    })
  }

  userInfo(id) {
    this.selectedUser = this.users.find(user => user._id === id);
  }

}
