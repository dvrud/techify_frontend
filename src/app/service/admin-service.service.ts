import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {appConfig} from '../app.config';
import {map} from 'rxjs/operators';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

// const headers = new HttpHeaders({'Content-Type':'application/json; charset=utf-8'});

export class AdminServiceService {

  constructor(private http: HttpClient) { }

  login(data) {
    return this.http.post<any>(`${appConfig.apiUrl}/login`, data);
  }

  getUser() {
    return this.http.post<any>(`${appConfig.apiUrl}/user`, {}, {
      headers: {'access-token': localStorage.getItem('token')}
    })
  }

  getUsers() {
    return this.http.post<any>(`${appConfig.apiUrl}/users`, {}, {
      headers: {'access-token': localStorage.getItem('token')}
    })
  }
 
  registerUser(data) {
    return this.http.post<any>(`${appConfig.apiUrl}/createUser`, data)
  }

}
