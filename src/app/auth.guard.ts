import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivate,
  Router
} from "@angular/router";
import { Observable } from "rxjs";
import { AdminServiceService } from "../app/service/admin-service.service";

@Injectable({
  providedIn: "root"
})
export class AuthGuard implements CanActivate {
  tank: any;
  constructor(private auth: AdminServiceService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (!localStorage.getItem("token")) {
      localStorage.clear();
      this.router.navigate(["/"]);
      return false;
    } else {
      return true;
    }
  }
}
